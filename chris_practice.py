import numpy as np
import pandas as pd
import random as rand
import scipy as sp

'''

Practice python file for Chris' code

'''

# Reads voltage inputs from hardware into python for 1 min and gives 100 data points of voltage:

constant_current = .5
volt_raw = []  # Empty list

for n in range(0,100): # For each number between 0 and 100, create a random number
    n = rand.random()
    volt_raw.append(n) # Append each random value to the list voltage_raw

volt_raw_array = np.array(volt_raw) # Convert the list to an array
volt_avg = np.average(volt_raw_array) # Find the average value of the array
res_avg = volt_avg / constant_current # Convert from Voltage to Resistance using a constant current



# Turn all of the above into a function:

def data_collection(num_data_points,current): # num_data_points and current are parameters of the function. They can be changed each time
                                            # you call the function (see below)
    volt_raw = []
    for n in range(0,num_data_points):  # The range will depend on whatever number is used for the num_data_points parameter.
        n = rand.random()
        volt_raw.append(n)

    volt_raw = np.array(volt_raw)
    volt_avg = np.average(volt_raw)
    res_avg = volt_avg / current # The current will depend on whatever value is entered for the current parameter.

    return res_avg  # This is what we will get anytime we call the function (see below)


# Calling the function twice, using different parameters each time:

test1 = data_collection(50,.40)  # Calls the function data_collection, setting it equal to the variable test1.
                                        # Gave it parameters of 50 data points and current of 0.40 A
print(('Average Resistance of 50 samples = ') + str(test1) + (' with a constant current of .40 A'))

print('')

test2 = data_collection(10,.65)  # Same as above, but change the parameters of the function to 10 data points and .65 A
print(('Average Resistance of 10 samples = ') + str(test2) + (' with a constant current of .80 A'))

    
